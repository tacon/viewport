var gulp = require('gulp'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),
    streamify = require('gulp-streamify'),
    htmlreplace = require('gulp-html-replace'),
    reactify = require('reactify'),
    eslint = require('gulp-eslint');
//var uglify = require('gulp-uglify');

var path = {
  HTML: 'static-src/index.html',
  MINIFIED_OUT: 'build.min.js',
  MINIFIED_VENDOR: 'vendor.min.js',
  OUT: 'build.js',
  DEST: 'static',
  DEST_BUILD: 'static/js',
  DEST_SRC: 'static/src',
  ENTRY_POINT: './static-src/jsx/main.jsx',
  JSX_PATH: './static-src/jsx/**/*jsx',
  SASS_PATH: ['static-src/sass/app/**/*.scss', 'static-src/global/sass/**/*.scss'],
  CSS_BUILD: '/css/',
  CSS_DEST: 'static/css/',
  CSS_FILES: ['theme.css', 'main.css', 'font-faces.css', 'colors.css']
};

gulp.task('copy', function(){
  gulp.src(path.HTML)
    .pipe(gulp.dest(path.DEST));
});

/* Watcher tasks */
gulp.task('build:watch', ['build:jsx']);

gulp.task('watch', function() {
  gulp.watch(path.HTML, ['replaceHTML']);
  gulp.watch(path.JSX_PATH, ['build:watch']);
  gulp.watch(path.SASS_PATH, ['sass']);
  gulp.watch(['index.js', 'lib/*.js'], ['build:js']);
});

gulp.task('lint', function() {
  return gulp.src(path.JSX_PATH)
    .pipe(eslint({
      envs: [
        'node'
      ],
      rules: {
        quotes: [2, 'single']
      }
    }))
    .pipe(eslint.format());
});

gulp.task('replaceHTML', function(){
  return gulp.src(path.HTML)
    .pipe(htmlreplace({
      'js': '/js/' + path.MINIFIED_OUT,
      'vendorjs': '/js/' + path.MINIFIED_VENDOR,
      'css': path.CSS_FILES.map(function(file) {
        return path.CSS_BUILD + file;
      })
    }))
    .pipe(gulp.dest(path.DEST));
});

gulp.task('build:js', function() {
  return browserify({
    entries: ['./index.js']
  }).bundle()
    .pipe(source('vp.min.js'))
    .pipe(gulp.dest('build/'));
});

gulp.task('build:jsx', function(){
  return browserify({
    entries: [path.ENTRY_POINT],
    transform: [reactify]
  })
    .bundle()
    .pipe(source(path.MINIFIED_OUT))
//    .pipe(streamify(uglify(path.MINIFIED_OUT)))
    .pipe(gulp.dest(path.DEST_BUILD));
});

gulp.task('replaceHTML', function(){
  return gulp.src(path.HTML)
    .pipe(htmlreplace({
      'js': '/js/' + path.MINIFIED_OUT,
      'vendorjs': '/js/' + path.MINIFIED_VENDOR,
      'css': path.CSS_FILES.map(function(file) {
        return path.CSS_BUILD + file;
      })
    }))
    .pipe(gulp.dest(path.DEST));
});

gulp.task('default', ['watch']);
