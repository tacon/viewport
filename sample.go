package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/websocket"
	"github.com/ttacon/toml"
	"github.com/yext/glog"
)

var (
	// flags
	port       = flag.String("p", ":18091", "port to run websocket logger on")
	configFile = flag.String("config", "insite.toml", "cofiguration file")

	// global used variables, extra to store interfaces (pouch v0.2)
	dbConn *sql.DB

	// this is gross, but for now, use a single map to save session -> hosts
	seshMap         = make(map[string]string)
	hostToProcessor = make(map[string]chan *wsPacket)

	// prepared statements - cuz they're easier to use, no other reason :P
	insertSession *sql.Stmt

	// processing components
	toProcess chan *packet
	events    chan *wsPacket
)

func loadAndProcessConfig(confLoc string) error {
	dbytes, err := ioutil.ReadFile(confLoc)
	if err != nil {
		return err
	}

	// TODO(ttacon): extract concrete type
	var config struct {
		DB map[string]string `toml:"db"`
	}
	err = toml.Unmarshal(dbytes, &config)
	if err != nil {
		return err
	}

	// extra connection info for ... mysql?
	dbInfo := config.DB
	dbConn, err = sql.Open(
		"mysql",
		fmt.Sprintf(
			"%s:%s@%s/%s?parseTime=true",
			dbInfo["username"],
			dbInfo["password"],
			dbInfo["host"],
			dbInfo["database"],
		),
	)
	if err != nil {
		return nil
	}

	if err = dbConn.Ping(); err != nil {
		return err
	}

	insertSession, err = dbConn.Prepare(`
insert into Session (
  ID, Host, RequestURI
) values (
  ?, ?, ?
)
`)

	// TODO(ttacon): more config info goes here
	return nil
}

func main() {
	flag.Set("alsologtostderr", "true")
	flag.Parse()

	// load config and set up necessary components
	err := loadAndProcessConfig(*configFile)
	if err != nil {
		glog.Error("failed to load config file, err: ", err)
		os.Exit(1)
	}

	http.HandleFunc("/ws", wsHandler)
	http.HandleFunc("/ws-host", wsHostHandler)
	curr, err := os.Getwd()
	if err != nil {
		glog.Error("failed to get cwd: ", err)
		return
	}
	http.Handle("/", http.FileServer(http.Dir(curr)))

	toProcess = make(chan *packet, 2048)
	events = make(chan *wsPacket, 2048)
	go runProcessor(toProcess, events)
	go eventFilter(events)
	glog.Error(http.ListenAndServe(*port, nil))
}

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     func(r *http.Request) bool { return true },
	}
)

func wsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		glog.Error("failed to upgrade connection")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	seshSlug := randSlug()
	// save sesh slug, host and req URI
	// should we really ignore the result?
	_, err = insertSession.Exec(seshSlug, r.Host, r.RequestURI)
	if err != nil {
		glog.Error("failed to save user session, err: ", err)
		conn.Close()
		return
	}

	if err = conn.WriteMessage(1, []byte(`{"sid":"`+seshSlug+`"}`)); err != nil {
		glog.Error("failed to write session: ", err)
		conn.Close()
		return
	}
	seshMap[seshSlug] = r.Host
	if evProc, ok := hostToProcessor[r.Host]; ok {
		evProc <- &wsPacket{
			SID:       seshSlug,
			Action:    "hit",
			Timestamp: int(time.Now().Unix()),
		}
	}

	for {
		mType, p, err := conn.ReadMessage()
		if err != nil {
			fmt.Println("err: ", err)
			conn.Close()
			// this is a gross anti-pattern, allow passing conn creation
			// and deletion code to the main processor who will APPROPRIATELY
			// filter the right data to the right consumer
			if evProc, ok := hostToProcessor[r.Host]; ok {
				evProc <- &wsPacket{
					SID:       seshSlug,
					Action:    "closed",
					Timestamp: int(time.Now().Unix()),
				}
			}
			return
		}
		if mType != websocket.TextMessage {
			fmt.Println("mType: ", mType)
			continue
		}

		// TODO(ttacon): validate SID == seshSlug

		toProcess <- &packet{
			insiteData: p,
			extra: map[string]interface{}{
				"seshSlug": seshSlug,
			},
		}
	}
}

type packet struct {
	insiteData []byte
	extra      map[string]interface{}
}

func runProcessor(incoming <-chan *packet, events chan<- *wsPacket) {
	glog.Info("starting incoming...")
	// TEMP(ttacon): create log file for replay
	f, err := os.Create(time.Now().Format(time.RFC3339Nano) + ".jsonish")
	// not a fan, but again, better to take down early than to not work
	// TODO(ttacon): move to initial setup, and put under "create replay" flag
	if err != nil {
		panic(err)
	}

	newline := []byte("\n")

	// start dealing with events
	for d := range incoming {
		// d should b JSON
		var p wsPacket
		err := json.Unmarshal(d.insiteData, &p)
		if err != nil {
			glog.Error("failed to unmarshal packet: ", err)
			continue
		}
		fmt.Printf("got: %#v\n", p)
		f.Write(d.insiteData)
		f.Write(newline)

		// pass on insite data for realtime updates for users
		events <- &p
	}
	glog.Info("closed incoming...")
}

func eventFilter(events <-chan *wsPacket) {
	glog.Info("event filter running...")
	for e := range events {
		host, ok := seshMap[e.SID]
		if !ok {
			continue
		}

		if consumer, ok := hostToProcessor[host]; ok {
			consumer <- e
		}
	}
	glog.Info("event filter stopped...")
}

// once we add more packet types, embed extra data (such as new scroll top)
type wsPacket struct {
	SID          string `json:"sid"`
	Action       string `json:"a"`
	Timestamp    int    `json:"t"`
	NewScrollTop int    `json:"nst,omitempty"`
}

var posss = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func randSlug() string {
	var slug = ""
	for i := 0; i < 25; i++ {
		if i%5 == 0 && i != 0 {
			// faster to just split/add "-" at end?
			// every ns counts...
			slug += "-"
		}
		slug += string(posss[rand.Intn(36)])
	}
	fmt.Println("returning: ", slug)
	return slug
}

func init() {
	// TODO(ttacon): yes, this is horrible, but right now there is a lot more
	// to take care of than worrying about this
	rand.Seed(time.Now().UnixNano())
}

func wsHostHandler(w http.ResponseWriter, r *http.Request) {
	host := r.URL.Query().Get("host")
	if len(host) == 0 {
		glog.Error("no host provided for ws-host")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		glog.Error("failed to upgrade conection to websocket, err: ", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// TODO(ttacon): deal with multiple users wanted to watch their users
	// in real time
	events := make(chan *wsPacket, 256)
	hostToProcessor[host] = events

	for e := range events {
		eJSON, err := json.Marshal(e)
		if err != nil {
			glog.Error("failed to marshal json to send to watcher, err: ", err)
			continue
		}
		if err = conn.WriteMessage(1, eJSON); err != nil {
			// connection is probably lost, disconnect/destroy this channel,
			// they frontend will reconnect them
			delete(hostToProcessor, host)
			conn.Close()
		}
	}
}
