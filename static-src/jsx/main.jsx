'use strict';

var React = require('react/addons'),
    ReactRouter = require('react-router'),
	$ = require('jquery'),
	routes = require('./routes.jsx');

require('./preloader.jsx');



/*
var EntitiesStore = require('./stores/entities_store.jsx');
var TracksStore = require('./stores/tracks_store.jsx');
var PlaysStore = require('./stores/plays_store.jsx')

// TODO: Add other stores
var stores = {
  EntitiesStore: new EntitiesStore(),
  TracksStore: new TracksStore(),
  PlaysStore: new PlaysStore()
};
*/
/* Initializing touch events */
React.initializeTouchEvents(true);

Pace.once('hide', function() {
  $('#pace-loader').removeClass('pace-big').addClass('pace-small');
});

var InitializeRouter = function(View) {
  // cleanup
  Pace.restart();

  React.render(<View/>, document.getElementById('app-container'), function() {
    setTimeout(function() {
      $('body').removeClass('fade-out');
    }, 500);
  });
};

ReactRouter.run(routes, ReactRouter.HistoryLocation, InitializeRouter);
