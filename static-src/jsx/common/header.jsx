var React = require('react');
var ReactRouter = require('react-router');
var rutil = require('./rutil.js');
var Bootstrap = require('react-bootstrap'),
	DropdownButton = Bootstrap.DropdownButton,
	MenuItem = Bootstrap.MenuItem,
	NavItem = Bootstrap.NavItem,
	Navbar = Bootstrap.Navbar,
	Nav = Bootstrap.Nav,
	Grid = Bootstrap.Grid,
	Row = Bootstrap.Row,
	Col = Bootstrap.Col;

var Header = React.createClass({
  render: function() {
    return (
      <Grid {...this.props} id='navbar'>
        <Row>
          <Col xs={12}>
            <Navbar brand='Project Insite'>
              <Nav>
                <DropdownButton title='Events'>
                  <MenuItem>Live Map</MenuItem>
                  <MenuItem>Digested</MenuItem>
                  <MenuItem>Raw</MenuItem>
                </DropdownButton>
                <NavItem>Settings</NavItem>
              </Nav>
            </Navbar>
          </Col>
        </Row>
      </Grid>
    );
  }
});

module.exports = Header;
