var mergeProps = function(oldProps, newProps) {
	return Object.assign({}, oldProps, newProps);
};

var isObject = function(probj) {
  return probj !== null && !Array.isArray(probj) && typeof probj === 'object';
};

module.exports = {
  mergeProps: mergeProps,
  isObject: isObject
};
