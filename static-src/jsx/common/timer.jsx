var React = require('react');
var moment = require('moment');

// Basically taken from: http://facebook.github.io/react/ -- "A Stateful Component"

var Timer = React.createClass({
	propTypes: {
		initialElapsed: React.PropTypes.number,
		initialStart: React.PropTypes.string,
		clock: React.PropTypes.bool
	},
	getInitialState: function() {
		var startTime;
		if (this.props.initialStart) {
			startTime = moment(this.props.initialStart, 'MM/DD/YYYY HH:mm:ss');
		} else {
			startTime = moment();
		}
		return {
			secondsElapsed: this.props.initialElapsed || 0,
			startTime: startTime
		};
	},
	tick: function() {
		this.setState({
			secondsElapsed: this.state.secondsElapsed + 1,
			startTime: this.state.startTime.add(1, 'seconds')
		});
	},
	componentDidMount: function() {
		this.interval = setInterval(this.tick, 1000);
	},
	componentWillUnmount: function() {
		clearInterval(this.interval);
	},
	render: function() {
		if (this.props.clock || this.props.initialStart) {
			var d = this.state.startTime;
			return (
				<span>
					{d.month() + 1}/{d.date()}/{d.year()} {d.hour()}:{d.minute()}:{d.second()}
				</span>
			);
		}
		return (
			<span>{this.state.secondsElapsed}</span>
		);
	}
});


module.exports = Timer;
