var React = require('react');
var Bootstrap = require('react-bootstrap'),
	Grid = Bootstrap.Grid,
	Row  = Bootstrap.Row,
    Col  = Bootstrap.Col;

var ApplicationSidebar = React.createClass({
  render: function() {
    return (
      <div>
        <Grid>
          <Row>
            <Col xs={12}>
              <div className='sidebar-header'>PAGES</div>
              <div className='sidebar-nav-container'>
                <ul>
                  <li>
                    Dashboard
                  </li>
                  <li>
                    Events
                    <ul>
                      <li>Summary</li>
                      <li>Processed</li>
                      <li>Raw</li>
                    </ul>
                  </li>
                  <li>
                    System/Prefs
                    <ul>
                      <li>User Info</li>
                      <li>Billing Info</li>
                      <li>System Info</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
});

var SidebarSection = React.createClass({
  render: function() {
    return (
      <div id='sidebar' {...this.props}>
        <div id='avatar'>
          <Grid>
            <Row className='fg-white'>
              <Col xs={8} collapseLeft id='avatar-col'>
                <div style={{top: 19, fontSize: 16, lineHeight: 1, position: 'relative', color: '#ffffff'}}>
                  Project Insite
                </div>
              </Col>
            </Row>
          </Grid>
        </div>
        <div>
          <ApplicationSidebar />
        </div>
      </div>
    );
  }
});

module.exports = SidebarSection;
