var React = require('react/addons'),
    rutil = require('./rutil.js'),
    Bootstrap = require('./bootstrap/index.jsx');
var Input = Bootstrap.Input,
    Select = Bootstrap.Select;


var Editable = React.createClass({
  // this will also support select boxes later on
  propTypes: {
    initialValue: React.PropTypes.string,
    text: React.PropTypes.bool,
    initialOpen: React.PropTypes.bool
  },
  componentDidMount: function() {
    // again, this is hardcoding the ref to focus on, should be help in state
  },
  getInitialState: function() {
    return {
      editOpen: this.props.initialOpen,
      value: this.props.initialValue || this.props.value
    };
  },
  openEdit: function(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.setState({
      editOpen: true
    });
  },
  onBlur: function(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.closeInput();
  },
  onChange: function() {
    if (this.props.type === 'text') {
      this.setState({
        value: this.refs.input.getValue()
      });
    } else if (this.props.type === 'select') {
      var selected = this.refs.select.getSelected();
      if (selected.length) {
        selected = selected[0];
      } else {
        selected = '';
      }
      this.setState({
        value: selected
      });
    }
  },
  getEditableInput: function() {
    if (this.state.type === 'text') {
      return this.refs.input;
    } else if (this.state.type === 'select') {
      return this.refs.select;
    }
    return null;
  },
  closeInput: function() {
    if (this.props.whenDone) {
      this.props.whenDone(this.state.value);
    }
    this.setState({
      editOpen: false
    });
  },
  maybeHitEnter: function(evt) {
    if (evt.keyCode === 13 || evt.key === 'Enter') {
      evt.preventDefault();
      evt.stopPropagation();
      this.closeInput();
      return;
    }
    this.onChange();
  },
  swallowTab: function(evt) {
    if (evt.keyCode === 9) {
      evt.preventDefault();
      evt.stopPropagation();
      this.closeInput();
    }
  },
  render: function() {
    if (!this.state.editOpen) {
      var props = rutil.mergeProps({
        className: 'editable',
        onClick: this.openEdit
      }, this.props);
      if (!this.state.value) {
        return (
          <span {...props}>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </span>
        )
      }
      return (
        <span {...props}>
          {this.state.value}
        </span>
      );
    }
    if (this.props.type === 'text') {
      var props = rutil.mergeProps({
        ref: 'input',
        onBlur: this.onBlur,
        type: 'text',
        autoFocus: true,
        onChange: this.onChange,
        onKeyDown: this.swallowTab,
        onKeyPress: this.maybeHitEnter
      }, this.props);
      props.value = this.state.value;
      return (
        <span>
          <Input {...props} />
        </span>
      );
    } else if (this.props.type === 'select') {
      var options = [];
      var opList = this.props.options;
      for (var i=0; i < opList.length; i++) {
        options.push(
          (
            <option key={opList[i]} value={opList[i]}>
              {opList[i]}
            </option>
          )
        );
      }
      var props = rutil.mergeProps({
        ref: 'select',
        onBlur: this.onBlur,
        autoFocus: true,
        onChange: this.onChange,
        onKeyDown: this.swallowTab,
        onKeyPress: this.maybeHitEnter
      }, this.props);
      props.value = this.state.value;
      return (
        <Select sm {...props}>
          {options}
        </Select>
      );
    }
    return (
      <span>Unknown editable type!</span>
    );
  }
});

module.exports = Editable;
