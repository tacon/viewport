var Header = require('./../common/header.jsx');
var Sidebar = require('./../common/sidebar.jsx');
var Footer = require('./../common/footer.jsx');
var Bootstrap = require('react-bootstrap'),
	Grid = Bootstrap.Grid,
	Row  = Bootstrap.Row,
    Col  = Bootstrap.Col;

var React = require('react/addons');

var Body = React.createClass({
  render: function() {
	rows = [];
    return (
      <div id='body'>
        <Grid>
          <Row>
            <Col sm={12}>
              <h1>User Events</h1>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
});

var Page = React.createClass({
  render: function() {
    return (
      <Grid id='container'>
        <Row>
          <Header />
        </Row>
        <Row>
          <Body />
        </Row>
        <Row>
          <Footer />
        </Row>
      </Grid>
    );
  }
});

module.exports = Page;
