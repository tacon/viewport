var Header = require('../common/header.jsx');
var Sidebar = require('../common/sidebar.jsx');
var Footer = require('../common/footer.jsx');
var Bootstrap = require('react-bootstrap'),
    Grid = Bootstrap.Grid,
	Row  = Bootstrap.Row,
    Col  = Bootstrap.Col;

var React = require('react');

var Body = React.createClass({
  render: function() {
    return (
      <div id='body'>
        <Grid gutterBottom>
          <Row>
            <Col sm={12} className='text-center'>
              <div>
                <Grid>
                  <Row>
                    <Col xs={12}>
                      <h1 style={{marginBottom: 25, marginTop: 0}}>Page not found!</h1>
                    </Col>
                  </Row>
                </Grid>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
});

var classSet = React.addons.classSet;
var PageNotFound = React.createClass({
  render: function() {
    return (
      <Grid id='container'>
        <Row>
          <Header />
        </Row>
        <Row>
          <Body />
        </Row>
        <Row>
          <Footer />
        </Row>
      </Grid>
    );
  }
});

module.exports = PageNotFound;
