var index = require('./routes/index.jsx'),
    notFound = require('./routes/notFound.jsx'),
    dash = require('./routes/dash.jsx');


var React = require('react');
var Router = require('react-router');
var Route = Router.Route,
		DefaultRoute = Router.DefaultRoute,
		NotFoundRoute = Router.NotFoundRoute,
		RouteHandler = Router.RouteHandler;

var routes = (
  <Route handler={RouteHandler}>
    <DefaultRoute handler={index} />
    <Route path='/' handler={index} />

    <Route path='/dash' handler={dash} />
    
    <NotFoundRoute handler={notFound} />
  </Route>
);

module.exports = routes;
