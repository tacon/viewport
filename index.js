var hidden, visibilityChange; 
if (typeof document.hidden !== "undefined") {
  // Opera 12.10 and Firefox 18 and later support 
  hidden = "hidden";
  visibilityChange = "visibilitychange";
} else if (typeof document.mozHidden !== "undefined") {
  hidden = "mozHidden";
  visibilityChange = "mozvisibilitychange";
} else if (typeof document.msHidden !== "undefined") {
  hidden = "msHidden";
  visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
  hidden = "webkitHidden";
  visibilityChange = "webkitvisibilitychange";
}

var Viewport = function(el, cbs) {
  this._el = el;
  this._cbs = cbs;
};

Viewport.prototype = {
  start: function(){
    var _this = this;

    _this.ws = new WebSocket('ws://localhost:18091/ws');
    //_this.ws.onclose(_this.reopn);
    _this.ws.onmessage = function(evt) {
      if (!evt.data) {
        console.log('conn carried no sid, ask for one (buffer events until then)');
      }
      var data = JSON.parse(evt.data);
      _this.sid = data.sid;
      // TODO(ttacon): move session "management" to own functionality
      if (window.localStorage) {
        window.localStorage.setItem('vp::cs', JSON.stringify([_this.sid]));
      } else {
        console.warn('failed to identify open sessions');
      }
    };
    // we want to track how the user scrolls
    window.addEventListener('resize', function(evt) {
      console.log('==========RESIZE');
      console.log(evt);
      console.log('==========');
    });
    document.addEventListener('scroll', function(evt) {
      _this._onScroll(evt);
    });

    // we want to track when the user leaves/returns to the page
    if (document) {
      document.addEventListener(visibilityChange, function() {
        if (document[hidden]) {
          _this._userLeftPage();
        } else {
          _this._userReturnedToPage();
        }
      }, false);
    }

    // TODO(ttacon): get the correct polyfill
    if (window.localStorage) {
      var currentSessions = window.localStorage.getItem('vp::cs');
      try {
        console.log(JSON.parse(currentSessions));
      } catch (ex) {
        console.error(ex);
      }
    }
  },
  _userLeftPage: function() {
    var _this = this;
    // console.log('user left the page!');
    this.ws.send(JSON.stringify({
      sid: _this.sid,
      a: 'ulp',
      t: (new Date()).getTime()
    }));
  },
  _userReturnedToPage: function() {
    // console.log('user returned to page');
    var _this = this;
    this.ws.send(JSON.stringify({
      sid: _this.sid,
      a: 'ur2p',
      t: (new Date()).getTime()
    }));
  },
  _doneScrolling: function() {
    var _this = this;
    console.log(document.body.scrollTop);
    // nst ---> newScrollTop
    this.ws.send(JSON.stringify({
      sid: _this.sid,
      a: 'scroll',
      nst:document.body.scrollTop,
      t: (new Date()).getTime()
    }));
  },
  _scrollTimer: -1,
  _onScroll: function() {
    var _this = this;
    if (_this.scrollTimer !== -1) {
      clearTimeout(_this.scrollTimer);
    }
    _this.scrollTimer = setTimeout(_this._doneScrolling.bind(_this), 150);
  },
  _fireNotifications: function(viewMove) {
    if (!this._cbs) { return; }
    for (var i=0; i < this._cbs.length; i++) {
      this._cbs[i](viewMove);
    }
  }
};

module.exports = Viewport;

// for testing at the moment
window.Viewport = Viewport;
var v = new Viewport();
v.start();
